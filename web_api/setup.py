from setuptools import setup, find_packages


_NAME = 'web_api'

setup(
    name=_NAME,
    version='0.0.1',
    packages=find_packages(),
    python_requires='>=3.10.0',
    install_requires=[
        line for line in open('requirements.txt', 'r').readlines()
        if line and not line.startswith('-')
    ],
)
