import functools
import os

import grpc # type: ignore

from web_api.protos import meter_pb2_grpc as meter_rpc # type: ignore


@functools.cache
def get_meter_api_client():
    hostname = os.getenv('METER_API_HOSTNAME', 'localhost')
    port = os.getenv('METER_API_PORT', '80')

    return meter_rpc.MeterStub(grpc.insecure_channel(f'{hostname}:{port}'))
