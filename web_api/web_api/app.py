import math

from datetime import datetime
from typing import List

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel

from web_api.meter_api import get_meter_api_client
from web_api.protos import meter_pb2 as meter_protos # type: ignore


app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


class Reading(BaseModel):
    timestamp: datetime
    value: float


class Readings(BaseModel):
    readings: List[Reading]


@app.get('/api/v1/readings/{begin}/{end}', response_model=Readings)
def get_readings(begin: datetime, end: datetime):
    meter = get_meter_api_client()
    time_span = meter_protos.TimeSpan(
        begin=begin.isoformat(),
        end=end.isoformat(),
    )
    response = meter.GetReadings(time_span)
    return Readings(
        readings=[ # yapf: disable
            Reading(
                timestamp=datetime.strptime(
                    reading.timestamp, '%Y-%m-%dT%H:%M:%S%z'
                ),
                value=reading.value
            )
            for reading in response.readings
            # Sometimes readings are INF/NaN. We can't meaningfully display
            # them, so just act like we don't have this data.
            if not (math.isinf(reading.value) or math.isnan(reading.value))
        ]
    )
