import csv
import operator

from bisect import bisect_left, bisect_right
from datetime import datetime, timezone
from functools import total_ordering
from pathlib import Path
from typing import List

from meter_api.time_span import TimeSpan


@total_ordering
class MeterReading:

    """
    MeterReading represents a meter reading at a point in time
    """

    __slots__ = ('datetime', 'value')

    def __init__(self, dt: datetime, value: float):
        self.datetime: datetime = dt
        self.value: float = value

    def __eq__(self, other) -> bool:
        # __eq__'s `other` parameter is typing.Any, so instead of overriding
        # that signature and annotating `other` as 'MeterReading', we return
        # NotImplemented for instances MeterReading can't be compared to
        if not isinstance(other, MeterReading):
            return NotImplemented

        return self.datetime == other.datetime and self.value == other.value

    def __lt__(self, other: 'MeterReading') -> bool:
        return self.datetime < other.datetime

    def __repr__(self):
        return f'MeterReading({self.datetime!r}, {self.value!r})'


class MeterReadings:

    """
    MeterReadings represents a series of meter readings (MeterReading
    instances). It also provides quick access to a range of meter
    readings by timestamp.
    """

    def __init__(self, readings: List[MeterReading]):
        self._readings: List[MeterReading] = sorted(readings)

    @classmethod
    def FromCSV(cls, csv_path: Path):
        with open(csv_path, 'r') as fobj:
            reader = csv.reader(fobj)
            next(reader) # skip header
            return cls(readings=[ # yapf: disable
                MeterReading(
                    dt=datetime.strptime(
                        timestamp,
                        '%Y-%m-%d %H:%M:%S'
                    ).replace(tzinfo=timezone.utc),
                    value=float(meter_usage)
                ) for timestamp, meter_usage in reader
            ])

    def get_readings(self, time_span: TimeSpan) -> List[MeterReading]:
        """
        Returns a sequence of readings inside a TimeSpan (inclusive).

        If the TimeSpan is regular, i.e. its beginning is before its
        end, get_readings returns readings from least to most recent.
        But if the TimeSpan is reversed, get_readings returns readings
        from most to least recent.
        """
        if time_span.is_empty:
            return []

        begin: datetime = time_span.begin
        end: datetime = time_span.end
        reverse: bool = time_span.is_reversed
        bisect_func = bisect_left

        if reverse:
            begin = time_span.end
            end = time_span.begin
            bisect_func = bisect_right

        # Using bisect here lets us avoid O(n) behavior where we'd have to look
        # through each member of a sorted list.
        start_index: int = bisect_func(
            self._readings, begin, key=operator.attrgetter('datetime')
        )
        end_index: int = bisect_func(
            self._readings,
            end,
            lo=start_index,
            key=operator.attrgetter('datetime')
        )

        readings_subset = self._readings[start_index:end_index]

        if reverse:
            readings_subset.reverse()

        return readings_subset

    def __repr__(self):
        return 'MeterReadings(readings=<readings>)'
