from datetime import datetime


class TimeSpan:

    """
    TimeSpan represents a span of time with a beginning and end.
    """

    __slots__ = ('begin', 'end')

    def __init__(self, begin: datetime, end: datetime):
        self.begin = begin
        self.end = end

    @classmethod
    def FromTimestamps(cls, begin: str, end: str):
        """
        Builds a TimeSpan from timestamps
        """
        return cls(
            datetime.strptime(begin, '%Y-%m-%dT%H:%M:%S%z'),
            datetime.strptime(end, '%Y-%m-%dT%H:%M:%S%z'),
        )

    @property
    def is_empty(self) -> bool:
        """
        Returns True if the TimeSpan's beginning is the same as its end
        """
        return self.begin == self.end

    @property
    def is_reversed(self) -> bool:
        """
        Returns True if the TimeSpan's beginning is after its end
        """
        return self.begin > self.end

    def __repr__(self):
        return f'TimeSpan({self.begin!r}, {self.end!r})'
