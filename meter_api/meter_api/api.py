from meter_api.protos import ( # type: ignore
    meter_pb2 as meter_protos,
    meter_pb2_grpc as meter_rpc
)
from meter_api.time_span import TimeSpan


class MeterAPI(meter_rpc.MeterServicer):

    def __init__(self, readings, *args, **kwargs):
        meter_rpc.MeterServicer.__init__(self, *args, **kwargs)
        self.readings = readings

    def GetReadings(self, request, context):
        time_span = TimeSpan.FromTimestamps(request.begin, request.end)
        return meter_protos.GetReadingsResponse( # yapf: disable
            readings=[
                meter_protos.MeterReading(
                    timestamp=r.datetime.isoformat(), value=r.value
                ) for r in self.readings.get_readings(time_span)
            ]
        )
