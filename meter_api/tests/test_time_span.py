from datetime import datetime

from meter_api.time_span import TimeSpan


def test_from_timestamps():
    ts = TimeSpan.FromTimestamps('2022-05-31 01:23:45', '2022-05-31 12:34:56')
    assert ts.begin == datetime(2022, 5, 31, 1, 23, 45)
    assert ts.end == datetime(2022, 5, 31, 12, 34, 56)


def test_normal():
    ts = TimeSpan(
        datetime(2022, 5, 31, 1, 23, 45), datetime(2022, 5, 31, 12, 34, 56)
    )
    assert not ts.is_empty
    assert not ts.is_reversed


def test_is_empty():
    ts = TimeSpan(
        datetime(2022, 5, 31, 1, 23, 45), datetime(2022, 5, 31, 1, 23, 45)
    )
    assert ts.is_empty
    assert not ts.is_reversed


def test_is_reversed():
    ts = TimeSpan(
        datetime(2022, 5, 31, 12, 34, 56), datetime(2022, 5, 31, 1, 23, 45)
    )
    assert ts.is_reversed
    assert not ts.is_empty
