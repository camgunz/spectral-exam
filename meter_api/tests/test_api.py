# pylint: disable=redefined-outer-name

import pytest

from meter_api.meter_reading import MeterReading, MeterReadings
from meter_api.protos import ( # type: ignore
    meter_pb2 as meter_protos,
    meter_pb2_grpc as meter_rpc
)
from meter_api.utils import timestamp_to_datetime


@pytest.fixture(scope='module')
def readings_list():
    return [
        MeterReading(
            timestamp_to_datetime('2019-01-01 00:15:00'), float(55.09)
        ),
        MeterReading(
            timestamp_to_datetime('2019-01-01 00:30:00'), float(54.64)
        ),
        MeterReading(
            timestamp_to_datetime('2019-01-01 00:45:00'), float(55.18)
        ),
        MeterReading(
            timestamp_to_datetime('2019-01-01 01:00:00'), float(56.03)
        ),
        MeterReading(
            timestamp_to_datetime('2019-01-01 01:15:00'), float(55.77)
        ),
        MeterReading(
            timestamp_to_datetime('2019-01-01 01:30:00'), float(55.45)
        ),
        MeterReading(
            timestamp_to_datetime('2019-01-01 01:45:00'), float(55.74)
        ),
        MeterReading(
            timestamp_to_datetime('2019-01-01 02:00:00'), float(55.8)
        ),
        MeterReading(
            timestamp_to_datetime('2019-01-01 02:15:00'), float(55.62)
        ),
        MeterReading(
            timestamp_to_datetime('2019-01-01 02:30:00'), float(55.45)
        )
    ]


@pytest.fixture(scope='module')
def grpc_add_to_server():
    return meter_rpc.add_MeterServicer_to_server


@pytest.fixture(scope='module')
def grpc_servicer(readings_list):
    # pylint: disable=import-outside-toplevel
    from meter_api.api import MeterAPI

    return MeterAPI(MeterReadings(readings_list))


@pytest.fixture(scope='module')
def grpc_stub(grpc_channel):
    return meter_rpc.MeterStub(grpc_channel)


def test_get_readings(grpc_stub):
    time_span = meter_protos.TimeSpan(
        begin='2019-01-01 01:15:00', end='2019-01-01 01:30:01'
    )
    response = grpc_stub.GetReadings(time_span)
    assert hasattr(response, 'readings')
    assert len(response.readings) == 2
    assert response.readings[0].timestamp == '2019-01-01T01:15:00Z'
    assert response.readings[0].value == 55.77
    assert response.readings[1].timestamp == '2019-01-01T01:30:00Z'
    assert response.readings[1].value == 55.45
