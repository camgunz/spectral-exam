# pylint: disable=redefined-outer-name

from datetime import datetime
from pathlib import Path

import pytest

from meter_api.meter_reading import MeterReading, MeterReadings
from meter_api.time_span import TimeSpan
from meter_api.utils import timestamp_to_datetime


CSV_DATA = """\
time,meterusage
2019-01-01 00:15:00,55.09
2019-01-01 00:30:00,54.64
2019-01-01 00:45:00,55.18
2019-01-01 01:00:00,56.03
2019-01-01 01:15:00,55.77
2019-01-01 01:30:00,55.45
2019-01-01 01:45:00,55.74
2019-01-01 02:00:00,55.8
2019-01-01 02:15:00,55.62
2019-01-01 02:30:00,55.45"""


@pytest.fixture
def readings_list():
    return [
        MeterReading(
            timestamp_to_datetime('2019-01-01 00:15:00'), float(55.09)
        ),
        MeterReading(
            timestamp_to_datetime('2019-01-01 00:30:00'), float(54.64)
        ),
        MeterReading(
            timestamp_to_datetime('2019-01-01 00:45:00'), float(55.18)
        ),
        MeterReading(
            timestamp_to_datetime('2019-01-01 01:00:00'), float(56.03)
        ),
        MeterReading(
            timestamp_to_datetime('2019-01-01 01:15:00'), float(55.77)
        ),
        MeterReading(
            timestamp_to_datetime('2019-01-01 01:30:00'), float(55.45)
        ),
        MeterReading(
            timestamp_to_datetime('2019-01-01 01:45:00'), float(55.74)
        ),
        MeterReading(
            timestamp_to_datetime('2019-01-01 02:00:00'), float(55.8)
        ),
        MeterReading(
            timestamp_to_datetime('2019-01-01 02:15:00'), float(55.62)
        ),
        MeterReading(
            timestamp_to_datetime('2019-01-01 02:30:00'), float(55.45)
        )
    ]


def test_csv_read(tmp_path):
    csv_path = tmp_path / Path('test_meter_usage.csv')
    with open(csv_path, 'w') as fobj:
        fobj.write(CSV_DATA)
    readings = MeterReadings.FromCSV(csv_path)
    # pylint: disable=protected-access
    assert len(readings._readings) == len(CSV_DATA.splitlines()) - 1 # header


def test_reading_count(readings_list):
    readings = MeterReadings(readings_list)
    # pylint: disable=protected-access
    assert len(readings._readings) == len(readings_list)


def test_get_readings(readings_list):
    readings = MeterReadings(readings_list)
    time_span = TimeSpan(
        datetime(2019, 1, 1, 0, 30, 0), datetime(2019, 1, 1, 1, 0, 0)
    )
    subset = readings.get_readings(time_span)
    assert len(subset) == 2
    assert subset[0].datetime == datetime(2019, 1, 1, 0, 30, 0)
    assert subset[0].value == 54.64
    assert subset[1].datetime == datetime(2019, 1, 1, 0, 45, 0)
    assert subset[1].value == 55.18


def test_get_readings_empty(readings_list):
    readings = MeterReadings(readings_list)
    time_span = TimeSpan(
        datetime(2019, 1, 2, 3, 4, 5), datetime(2019, 1, 2, 3, 4, 5)
    )
    subset = readings.get_readings(time_span)
    assert len(subset) == 0


def test_get_readings_reversed(readings_list):
    readings = MeterReadings(readings_list)
    time_span = TimeSpan(
        datetime(2019, 1, 1, 1, 0, 0), datetime(2019, 1, 1, 0, 30, 0)
    )
    subset = readings.get_readings(time_span)
    assert len(subset) == 2
    assert subset[0].datetime == datetime(2019, 1, 1, 1, 0, 0)
    assert subset[0].value == 56.03
    assert subset[1].datetime == datetime(2019, 1, 1, 0, 45, 0)
    assert subset[1].value == 55.18


def test_get_readings_wide_early(readings_list):
    readings = MeterReadings(readings_list)
    time_span = TimeSpan(
        datetime(2018, 12, 31, 23, 15, 0), datetime(2019, 1, 1, 0, 45, 0)
    )
    subset = readings.get_readings(time_span)
    assert len(subset) == 2
    assert subset[0].datetime == datetime(2019, 1, 1, 0, 15, 0)
    assert subset[0].value == 55.09
    assert subset[1].datetime == datetime(2019, 1, 1, 0, 30, 0)
    assert subset[1].value == 54.64


def test_get_readings_wide_late(readings_list):
    readings = MeterReadings(readings_list)
    time_span = TimeSpan(
        datetime(2019, 1, 1, 2, 15, 0), datetime(2019, 1, 1, 4, 0, 0)
    )
    subset = readings.get_readings(time_span)
    assert len(subset) == 2
    assert subset[0].datetime == datetime(2019, 1, 1, 2, 15, 0)
    assert subset[0].value == 55.62
    assert subset[1].datetime == datetime(2019, 1, 1, 2, 30, 0)
    assert subset[1].value == 55.45


def test_get_readings_reversed_wide_early(readings_list):
    readings = MeterReadings(readings_list)
    time_span = TimeSpan(
        datetime(2019, 1, 1, 0, 45, 0), datetime(2018, 12, 31, 23, 15, 0)
    )
    subset = readings.get_readings(time_span)
    assert len(subset) == 3
    assert subset[0].datetime == datetime(2019, 1, 1, 0, 45, 0)
    assert subset[0].value == 55.18
    assert subset[1].datetime == datetime(2019, 1, 1, 0, 30, 0)
    assert subset[1].value == 54.64
    assert subset[2].datetime == datetime(2019, 1, 1, 0, 15, 0)
    assert subset[2].value == 55.09


def test_get_readings_reversed_wide_late(readings_list):
    readings = MeterReadings(readings_list)
    time_span = TimeSpan(
        datetime(2019, 1, 1, 4, 0, 0), datetime(2019, 1, 1, 2, 0, 0)
    )
    subset = readings.get_readings(time_span)
    assert len(subset) == 2
    assert subset[0].datetime == datetime(2019, 1, 1, 2, 30, 0)
    assert subset[0].value == 55.45
    assert subset[1].datetime == datetime(2019, 1, 1, 2, 15, 0)
    assert subset[1].value == 55.62
