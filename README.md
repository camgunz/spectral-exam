# Meter Readings

## Running the gRPC server (`meter_api`)

The `meter_api` folder contains the `meter_api` gRPC server.

To run the development server:
- Create and enter a virtualenv
- Enter the `meter_api` folder (same folder as its `Makefile`)
- Run `make server`

To create a Docker image and run the Dockerized server:
- Create and enter a virtualenv (you can reuse the virtualenv from the dev
  server)
- Enter the `meter_api` folder
- Run `make dockerimage`
- Run `make dockerserver`

## Running the web server (`web_api`)

The `web_api` folder contains the `web_api` server.

To run the development server:
- Create and enter a virtualenv (**should not** be the same as the virtualenv
  from `meter_api`)
- Enter the `web_api` folder
- Run `make server`

To create a Docker image and run the Dockerized server:
- Create and enter a virtualenv
- Enter the `web_api` folder
- Run `make dockerimage`
- Run `make dockerserver`

## Running the single-file web app

Simply pointing a browser at the file (e.g.
`file:///Users/charlie/code/spectral/index.html`) will load the web app.

## Ports

`meter_api`'s port can be configured in its `Makefile`.

`web_api`'s port can be configured in its `Makefile`. If you changed
`meter_api`'s ports, you'll also need to update `web_api`'s `docker.env` file
and update `METER_API_PORT`.

If you changed `web_api`'s ports, you'll also need to update `web_api`'s
`API_HOST` variable near the top of the `<script type="module">` tag.

## Dependency installation

The `pip` command in the `Makefile`s turns off binary downloading for `grpcio`
and `grpcio-tools` to work around a
[bug](https://github.com/grpc/grpc/issues/28387) with the `grpcio` wheel for
Python 3.10 on macOS.
